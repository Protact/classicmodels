Pour la suite en PHP :

Commencer à regarder du côté de la connexion vers les bases de données avec la librairie PDO (PHP Data Object).
Il faut bien analyser et comprendre les étapes suivantes :
1. Connexion au serveur : création d'un objet représentant la connexion
2. Préparation de la requête : obtention d'un objet représentant l'état de la requête
3. Exécution de la requête
4. Récupération d'un jeu d'enregistrement complet (représentent le résultat de la requête sous la forme d'un tableau associatif).

Le but est d'afficher une page HTML qui affiche sous la forme d'un tableau HTML toutes les commandes présentes dans la base de données.

L'étape suivant consistera à pouvoir cliquez sur un numéro de commande pour afficher le bon de commande associé.
Affichage des commandes (les filtres de tri et recherche ne sont pas utiles pour le moment)
Affichage du bon de commande
fabien

Doc PDO :

    https://www.php.net/manual/fr/book.pdo.php

Première étape à voir : Connexion et gestion de connexion

    https://www.php.net/manual/fr/pdo.connections.php
    https://www.php.net/manual/fr/pdo.construct.php

Préparation d'une requête :

    https://www.php.net/manual/fr/pdo.prepared-statements.php
    https://www.php.net/manual/fr/pdo.prepare.php (l'exemple dans cette page met tout en oeuvre : connexion, preparation, liage (bind), execution, fetch !)

Et pour revenir sur notre projet et l'étape de la création du bon de commande.

Vous devez créer une page order.php. Cette page, ou dans un jargon plus technique un controlleur a pour vocation de préparer toutes les données pour que la vue  puisse fournir le bon de commande au format HTML de la commande spécifiée.
Pour savoir quelle commande afficher, cette page doit recevoir un paramètre dans la queryString. Ce paramètre ne peut-être que l'identifiant unique d'une commande (la clef primaire ou primaryKey). Donc l'appel à notre page (le lien sur le numéro de commande dans notre liste de commande) sera par exemple order.php?id=10100

La priorité dans votre page order.phpc'est de récupérer cette identifiant. Une fois ces deux étapes terminée (création du lien avec le bon identifiant dans la liste des commande et récupération de cette identifiant dans notre page), il va vous falloir aller chercher les données dans la base :

    1. Le client qui a passé cette commande et les données de la commande (date de commande)
    2. Les détails de la commande (liste des produits commandés)
    3. La total de la commande.

Une fois ces données récupérées à l'aide de requête SQL la vue se charge de les afficher au bon endroit dans l'HTML.

Pour faire ces requêtes nous allons devoir lier notre paramètre reçu dans la queryString avec notre requête et ceci de façon sécurisée. D'où l'utilité de préparer notre requête avec un jeton, puis de lier ce jeton avec notre paramètre.
Voici un exemple, si notre contrôleur est appelé avec cette requête : order.php?id=10100 :

```php
//L'id a été reçu dans la chaine de requête
if (isset($_GET['id'])) {
// on se connecte 
$dbh = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// on prépare la requête avec un jeton (:orderNumber) qui sera lié avec l'id reçu
$sth = $dbh->prepare('SELECT * FROM orders WHERE orderNumber = :orderNumber');

// on lie ce jeton avec l'id
$sth->bindValue('orderNumber', $_GET['id'];

// on exécute la requête
$sth->execute();

// on récupère la valeur (ici on a qu'une résultat, une seul commande, donc on utilise fetch pour récupérer une seule ligne)
$order = $sth->fetch();
```
}