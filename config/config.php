<?php

// On dit à PHP d'afficher les erreurs pour ce script
ini_set('display_errors', '1');
// On dit à PHP d'afficher les erreur de démarrage pour ce script
ini_set('display_startup_errors', '1');
// On dit à PHP de nous reporter toutes les erreurs 
error_reporting(E_ALL);

/** Paramètres de connexion Mysql */
const DB_DSN = 'mysql:host=localhost;dbname=classicModels;charset=utf8';
const DB_USER = 'root';
const DB_PASSWORD = 'root';