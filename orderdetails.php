<?php

declare(strict_types=1);

/* On inclu la confioguration */
require('config/config.php');


/* Variable générique pour le layout */
const LAYOUT_VIEW = 'order/details';
const LAYOUT_TITLE = 'Bon de commande';

try 
{
    //L'id a été reçu dans la chaine de requête
    if (isset($_GET['id'])) 
    {
        // on se connecte 
        $dbh = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        // on prépare la requête avec un jeton (:productCode) qui sera lié avec l'id reçu
        $sth = $dbh->prepare('SELECT * FROM orderdetails od INNER JOIN products p ON(p.productCode = od.productCode) WHERE od.orderNumber = :orderNumber');
    
        // on lie ce jeton avec l'id
        $sth->bindValue('orderNumber', $_GET['id']);
    
        // on exécute la requête
        $sth->execute();
    
        // on récupère la valeur (ici on a qu'une résultat, une seul commande, donc on utilise fetch pour récupérer une seule ligne)
        $orders = $sth->fetchAll(PDO::FETCH_ASSOC);

        // // on prépare la requête avec un jeton (:productCode) qui sera lié avec l'id reçu
        // $sth = $dbh->prepare('SELECT * FROM orders o INNER JOIN customers c ON(c.customerNumber = o.customerNumber) WHERE o.orderNumber = :orderNumber');
    
        // // on lie ce jeton avec l'id
        // $sth->bindValue('customerNumber', $_GET['id']);
    
        // // on exécute la requête
        // $sth->execute();
    
        // // on récupère la valeur (ici on a qu'une résultat, une seul commande, donc on utilise fetch pour récupérer une seule ligne)
        // $orders = $sth->fetch(PDO::FETCH_ASSOC);
    }
    
    /** Inclu le layout */
    require('tpl/layout.phtml');
}
catch (PDOException $e) 
{
    echo 'Navré notre serveur de BDD est down !';
    echo $e->getMessage();
}

?>